﻿#include <iostream>
using namespace std;
#include <time.h>


int main()
{

    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    int date = buf.tm_mday;

    int sum = 0;

    const int size = 5;

    int divRem = date % size;

    int array[size][size];


    for (int i = 0; i < size; ++i)
    {
        for (int j = 0; j < size; ++j)
        {
            array[i][j] = i + j;
            cout << array[i][j] << " ";
        }
        cout << endl;
    }
    for (int j = 0; j < size; ++j)
    {
        sum = sum + array[divRem][j];
    }

    cout << "division remainder: " << divRem << '\n';

    cout << "line amount: " << sum;

    return 0;

}
